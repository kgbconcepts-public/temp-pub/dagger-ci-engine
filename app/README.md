## Dagger CI - Readme

**Welcome to Dagger CI!**

This repository implements Dagger CI, a tool for managing and automating your continuous integration pipelines.

### Features

* Define configuration options for the application using environment variables and a YAML file.
* Leverage informative logging for configuration loading and potential errors.
* Manage CI pipelines (e.g., define pipeline stages, execute jobs, generate reports).  ### Getting Started

**Prerequisites:**

* Go 1.17 or higher (Refer to [https://go.dev/doc/install](https://go.dev/doc/install) for installation instructions)
* A code editor or IDE of your choice

**Installation**

1. Clone the repository:

```bash
git clone https://gitlab.com/kgbconcepts-public/temp-pub/dagger-ci-engine.git
```

2. Navigate to the project directory:

```bash
cd dagger-ci-engine
```

3. Run the application:

```bash
go run config.go main.go
```

**Configuration**

Dagger CI utilizes a flexible configuration approach. You can define default values in the code, override them with environment variables, and manage centralized settings in a YAML file.

* **Default Values:** The `config.go` file defines a `Config` struct with default values for various configuration options.

* **Environment Variables:** You can set environment variables to override default values. For example, to set the HTTP port to a different value, you could use:

```bash
export DAGCI_CONFIG=./config.yaml # Path to your configuration file (optional)
export HTTP_PORT=8081 
go run main.go
```

* **YAML Configuration File:** The code attempts to load configuration from a YAML file at the specified path (`defaultPath` in `config.go`). This file can contain key-value pairs corresponding to configuration options. An example YAML file (`config.go`) might look like this:

```yaml
http_port: 9000  # Override default HTTP port
# Other configuration options can be defined here
# For example:
# database_url: "postgres://user:password@localhost:5432/dagger_ci_db"
# job_timeout: 600  # Job execution timeout in seconds (default: 300)
```

**Building**

To build a standalone executable of Dagger CI:

```bash
go build -o dagger-ci-engine config.go main.go
```

This will create an executable file named `dagger-ci-engine` in the current directory.


### Contributing

We welcome contributions to Dagger CI! Please refer to the `CONTRIBUTING.md` file for guidelines on submitting pull requests and code formatting conventions.


### Documentation

This repository utilizes the following documentation standards:

* **README.md:** This file provides an overview of the project, installation instructions, usage examples, and contribution guidelines.
* **config.go:** The code itself is well-documented with comments explaining the logic behind configuration loading and parsing.

### Improvements

## Improvements and Changes for `main.go` and `config.go`

Here's a consolidation of suggestions for improvements and changes to `main.go` and `config.go`, incorporating previous ideas:

**main.go:**

* **Error Handling:** Implement more granular error handling using different error types and appropriate handling mechanisms (e.g., log and exit for configuration errors, continue with defaults).
* **Configuration Usage:** Expand on how the application utilizes the configuration values retrieved from the `config` object. Showcase how these values influence the application's behavior.
* **Command Line Flags:** Consider adding command-line flags as an alternative way for users to provide configuration options during application startup.
* **Logging Levels:** Allow users to specify a different log level through environment variables or command-line flags, offering control over logging verbosity.
* **Documentation:** Improve code readability and maintainability by adding comments explaining the purpose of functions and variables within `main.go`.

**config.go:**

* **Configurability:** Extend the supported configuration options to encompass functionalities relevant to your CI pipeline management (e.g., database connection details, job timeout values, pipeline definition file paths).
* **Validation:** Implement validation checks to ensure loaded configuration values are within expected ranges or formats (e.g., validate port numbers are within valid ranges).
* **Security:** Explore secure handling of sensitive information in the configuration file (e.g., encryption, avoiding storing them in the file altogether).
* **Testing:** Add unit tests for configuration loading functions (`loadConfig`, `mergeConfig`) to ensure they work as expected under various scenarios (missing files, invalid YAML syntax).
* **Modularization:** As your configuration options grow, consider organizing them better using separate structs for different functionalities (e.g., `DatabaseConfig`, `JobConfig`) or a hierarchical configuration structure.
* **Flexibility:** Introduce a mapping mechanism between environment variable names and corresponding config fields to allow more flexibility in environment variable naming conventions.
* **Documentation:** Improve code readability by adding comments explaining the purpose of functions and variables within `config.go`.
* **Error Messages:** Enhance the detail and clarity of error messages returned by the configuration loading functions. This helps users troubleshoot configuration issues more effectively.

Config Schema Validation: You could explore libraries like go-valider or viper to define a schema for your configuration and validate the loaded YAML data against that schema for additional data integrity checks.

Separate Default Config File: Instead of defining default values directly in the code, consider creating a separate YAML file with default configurations. This allows for easier management and separation of concerns.

**General:**

* **Logging Configuration:** Explore integrating a more flexible logging configuration system that allows for dynamic changes to log levels and output formats.

**Additional Considerations:**

* As your project evolves, consider dependency injection for configuration management. This allows for easier testing and potential future extensions.
* Explore configuration management libraries like `viper` or `go-flags` that offer additional features and functionalities for managing application configuration in Go.

Remember, these are suggestions, and the  specific changes will depend on the specific functionalities and future direction of your Dagger CI project.

### License

This project is licensed under the MIT License: [https://opensource.org/license/mit](https://opensource.org/license/mit). See the `LICENSE` file for details.

