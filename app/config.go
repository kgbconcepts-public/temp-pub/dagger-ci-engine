package main

import (
	// Logging library
	"github.com/sirupsen/logrus"
	// YAML parsing library
	"gopkg.in/yaml.v3"
	// File operations
	"os"
	// Reflection for field access
	"reflect"
	// String manipulation
	"strings"
)

// **Config** struct to hold our configuration values
type Config struct {
	// HTTPPort defines the port for the HTTP server.
	Debug string `yaml:"debug"`
}

// defaultConfig provides default values used if environment variables or config file are not present.
var defaultConfig = Config{
	Debug: "false", // Set your default HTTP port here
}

// **GetConfig** function loads configuration from file or environment, falling back to in-memory defaults.
func GetConfig(configPathEnv string, defaultPath string, defaultConfig *Config) (*Config, error) {
	// Check for config path environment variable first
	if configPath := os.Getenv(configPathEnv); configPath != "" {
		config, err := loadConfig(configPath)
		if err != nil {
			logrus.Errorf("Error loading configuration from environment variable path: %w", err)
			return nil, err
		}
		return mergeConfig(config, configPathEnv), nil
	}

	// If no environment variable or missing file, use default config
	if _, err := os.Stat(defaultPath); os.IsNotExist(err) {
		logrus.Warnf("Config file not found at %s, using default configuration.", defaultPath)
		return defaultConfig, nil
	}

	// If environment variable not set but file exists, load and merge
	config, err := loadConfig(defaultPath)
	if err != nil {
		logrus.Errorf("Error loading configuration from file: %w", err)
		return nil, err
	}
	return mergeConfig(config, ""), nil
}

// **mergeConfig** function handles precedence between environment variables and config file.
func mergeConfig(config *Config, envPrefix string) *Config {
	// Loop through each field in the Config struct
	for i := 0; i < reflect.ValueOf(config).Elem().NumField(); i++ {
		field := reflect.ValueOf(config).Elem().Type().Field(i)
		fieldName := field.Name

		// Construct environment variable name based on prefix (if provided) and field name (uppercase)
		envVarName := envPrefix + strings.ToUpper(fieldName)

		// Check if environment variable is set
		if envValue := os.Getenv(envVarName); envValue != "" {
			// Override config field value with environment variable value
			reflect.ValueOf(config).Elem().FieldByName(fieldName).SetString(envValue)
		}
	}
	// Return the modified config struct
	return config
}

// **loadConfig** function handles actual file reading and parsing.
func loadConfig(filePath string) (*Config, error) {
	// Open the config file (optional as we might not have a file)
	data, err := os.ReadFile(filePath)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil // Not an error, just means file doesn't exist
		}
		logrus.Errorf("Error opening config file: %w", err)
		return nil, err
	}

	// Create a new Config struct
	config := &Config{}

	// Parse the YAML data (only if file exists)
	if data != nil {
		if err := yaml.Unmarshal(data, config); err != nil {
			logrus.Errorf("Error parsing YAML config: %w", err)
			return nil, err
		}
	}

	return config, nil
}

func init() {
	// **Configure logging** (replace with your desired settings)
	logrus.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
	})
	logrus.SetLevel(logrus.InfoLevel) // Set desired log level
}
