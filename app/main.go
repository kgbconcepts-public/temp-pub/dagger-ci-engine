package main

import (
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
)

func main() {
	// **Parse command-line flags** (including the env-prefix flag)
	flag.Parse()

	// **GetConfig** retrieves configuration values from environment variables, a YAML file, or defaults.
	config, err := GetConfig("DAGCI_CONFIG", "./config.yaml", &defaultConfig)
	if err != nil {
		logrus.Fatal(err) // Use logrus.Fatal for critical errors
	}

	fmt.Println("DEBUG:", config.Debug)

	// **Use other configuration values** from the config object (not shown in this snippet).
}
