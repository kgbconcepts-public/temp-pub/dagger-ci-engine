#!/bin/bash

# set debug
export DEBUG=${DEBUG:-FALSE}
if [ $DEBUG == true ]; then set -ex; fi

# set where we are running
export IS_PIPELINE=${IS_PIPELINE:-false}
export IS_SHELL_BUILD=${IS_SHELL_BUILD:-true}
# set what we use to build the app
export APP_BUILD_IMAGE=${APP_BUILD_IMAGE:-golang}
export APP_BUILD_IMAGE_TAG=${APP_BUILD_IMAGE_TAG:-latest}
# set some app related items
export APP_NAME=${APP_NAME:-dagger-ci-engine}
export APP_REPO=${APP_REPO:-"https://gitlab.com/kgbconcepts-public/temp-pub/$APP_NAME.git"}
export APP_BRANCH=${APP_BRANCH:-main}
# things on wrap up
export BUILD_CLEANUP=${BUILD_CLEANUP:-false}