#!/bin/bash

# get Go examples source code repository
SOURCE=$(dagger --debug query <<EOF | jq -r .git.branch.tree.id
{
  git(url:"$APP_REPO") {
    branch(name:"$APP_BRANCH") {
      tree {
        id
      }
    }
  }
}
EOF
)

# mount source code repository in golang container
# build Go binary
# export binary from container to host filesystem
BUILD=$(dagger --debug query <<EOF | jq -r .container.from.withDirectory.withWorkdir.withExec.file.export
{
  container {
    from(address:"$APP_BUILD_IMAGE:$APP_BUILD_IMAGE_TAG") {
      withDirectory(path: "/src", directory:"$SOURCE") {
        withWorkdir(path:"/src/app") {
          withExec(args:["go", "build", "-o", "$APP_NAME", "main.go"]) {
            file(path:"$APP_NAME") {
              export(path:"output/$APP_NAME")
            }
          }
        }
      }
    }
  }
}
EOF
)

# check build result and display message
if [ "$BUILD" == "true" ]
then
    echo "Build dagger ci engine successful"
else
    echo "Build dagger ci engine unsuccessful"
fi